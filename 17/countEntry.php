<?php
// Определяем количество вхождений элемента в массив
require_once 'functions.php';
$arrNumber = [1, 2, 3, 4, 5, 6, 7, 0, 23, 23,
              12, 16, 23, 96, 0, 28, 37, 54];      // массив чисел
$number = 23;                                      // число
<title>Количество вхождений</title>
echo '<p>Дан массив:</p>';
foreach ($arrNumber as $num) {
    echo $num . ' ';
}
echo '<br><br>';
// подсчет количества вхождений элемента в массив
echo 'Количество вхождений числа ' . $number . ' в массив - ' . countEntry($arrNumber, $number) . '.';
