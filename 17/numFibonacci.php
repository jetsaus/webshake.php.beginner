<?php
// Определяем четные числа в массиве
require_once ('functions.php');
echo '<title>Числа Фибоначчи</title>';
echo '<p>Вывести первые n чисел последовательности Фибоначчи.</p>';
$nFibonacci = 18;
echo '<p>Последовательность ' . $nFibonacci . ' чисел Фибоначчи:</p>';
$arrayFibonacci = numFibonacci($nFibonacci);
foreach ($arrayFibonacci as $item) {
    echo $item . ' ';
}