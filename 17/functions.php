<?php
/** Функция возвращает истину/ложь, если второй аргумент присутствует в массиве
 * @param array $arr    массив с набором элементов
 * @param int   $x      элемент, проверяемый на наличе в массиве
 * @return boolean
 */
function isPresent(array $arr, int $x) : bool
{
    $present = false;
    foreach ($arr as $num) {
        if ($num == $x) {
            $present = true;
            break;
        }
    }
    return $present;
}
/** Функция возвращает количество вхождений аргумента в массив
 * @param array $arr    массив с набором элементов
 * @param int   $x      элемент, проверяемый на вхождения массиве
 * @return int          количество вхождений
 */
function countEntry(array $arr, int $x) : int
{
    $count = 0;
    foreach ($arr as $num) {
        if ($num === $x) {
            $count++;
        }
    }
    return $count;
}
/** Функция возвращает массив последовательности Фибоначчи для положительных чисел
 * @param int       $n  количество чисел Фибоначчи
 * @return array        массив, содержащий последовательность Фибоначчи
 */
function numFibonacci(int $n) : array
{
    $arrF = [];                                     // массив чисел Фибоначчи
    for($i = 0, $j = 0; $i < $n; $i++, $j++) {
        if ($j<2) {
            $arrF[] = $i;
        }else{
            $arrF[] = $arrF[$j - 1] + $arrF[$j - 2];
        }
        //$arrF[] = ($j < 2) ? $i : $arrF[$j - 1] + $arrF[$j - 2];
    }
    return $arrF;
}
