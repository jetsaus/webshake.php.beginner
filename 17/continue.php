<?php
// Примеры continue в циклах
echo '<title>continue</title>';
echo 'Вывести все числа от 1 до 20, за исключением тех, что делятся на 3 без остатка.';
echo '<br><br>';
$isNumberFound = false;
for ($i=1;$i<=20;$i++) {
    if ($i % 3 != 0) {
        echo $i . ' ';
    }
}
