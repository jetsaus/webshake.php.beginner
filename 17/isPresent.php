<?php
// Проверяем присутствие элемента в массиве
<title>Присутствие элемента</title>
require_once 'functions.php';
$arrNumber = [1, 2, 3, 4, 5, 6, 7,
              12, 16, 23, 96, 0, 28, 37, 54];      // массив чисел
$number = 7;                                       // число
echo '<p>Дан массив:</p>';
foreach ($arrNumber as $num) {
    echo $num . ' ';
}
echo '<br>';
// проверка на прсутствие в массиве
if (isPresent($arrNumber, $number)) {
    echo 'Число ' . $number . ' присутствует в массиве.';
}else{
    echo 'Число ' . $number . ' отсутствует в массиве.';
}