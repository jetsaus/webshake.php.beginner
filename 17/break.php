<?php
// Примеры break в циклах
$array = [2, 3, 6, 1, 23, 2, 56, 7, 1, 15];
$number = 1;
echo '<title>break</title>';
echo 'Есть ли ' . $number . 'в массиве, или нет';
echo '<br><br>';
$isNumberFound = false;
foreach ($array as $item) {
    echo 'Сравниваем с числом элемент ' . $item . '<br>';
    if ($item === $number) {
        $isNumberFound = true;
        break;
    }
}
echo '<br>';
echo $isNumberFound ? 'Число найдено' : 'Число не найдено';
