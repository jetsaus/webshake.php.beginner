<?php
/* Уникальные числа */
$strNumStr = '1 16 56 1 3 63 56 12 3 7 16 7 63 12 5';   // строка с числами
$arrNumStr = [];                                        // массив чисел типа string
$arrNumInt = [];                                        // массив чисел типа integer
$arrNumUniq = [];                                       // массив уникальных чисел


echo '<title>Уникальные числа</title>';
echo '<p>На вход дана строка с числами, разделенными пробелами.Удалите все повторы чисел. Выведите их в любом порядке, разделив пробелами.</p>';
echo '<p>Строка с числами:</p>';
echo $strNumStr;
echo '<p>Решение:</p>';
$arrNumStr = explode(' ', $strNumStr);
foreach ($arrNumStr as $item) {                         // преобразуем массив элементов string в элементы integer
    $arrNumInt[] = (int)$item;
}
$arrNumUniq = array_unique($arrNumInt);                 // оставляем уникальные элементы
asort($arrNumUniq);                                     // сортируем по возрастанию
foreach ($arrNumUniq as $item) {                        // выводим результат
    echo $item . ' ';
}