<?php
/**
 * Некоторые функции для работы с массивами
 */

// проверка ключа
$array = [
    'login' => 'admin',
    'password' => 'пароль',
];
if (array_key_exists('password', $array)) {
    echo $array['password'];
} else {
    echo 'Ключ "password" в массиве не найден';
}
echo '<br><br>';

// Проверка существования значения
$numbers = [1, 0, 7, 4];
$needle = 12;
if (in_array($needle, $numbers)) {
    echo 'В массиве есть число ' . $needle;
}else{
    echo 'В массиве нет числа ' . $needle;
}
echo '<br><br>';

// Объединение массивов
$articlesFromIvan = [
    'Статья 1',
    'Статья 2',
];
$articlesFromMaria = [
    'Статья 3',
    'Статья 4',
];
$allArticles = array_merge($articlesFromIvan, $articlesFromMaria);
var_dump($allArticles);
