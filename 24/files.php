<?php
// Построчное чтение из файла
$file = fopen(__DIR__ . '/file.txt', 'r');
while (!feof($file)) {
    echo fgets($file);
    echo '<br>';
}
fclose($file);
// Запись в файл
$file = fopen(__DIR__ . '/file2.txt', 'w');
for ($i=1; $i<=100; $i++) {
    fwrite($file, $i . PHP_EOL);
}
fclose($file);
// Дозапись в конец файла
$file = fopen(__DIR__ . '/file3.txt', 'a');
fwrite($file, 'abc' . PHP_EOL);
fclose($file);
// Чтение файла целиком
$file = fopen(__DIR__ . '/file.txt', 'r');
$data = file_get_contents(__DIR__ . '/file.txt');
echo '<br>';
echo $data;
fclose($file);
// Запись в файл целиком
$data = 'abc' . PHP_EOL . 'def' . PHP_EOL;
file_put_contents(__DIR__ . '/file3.txt', $data);
// Дозапись в файл целиком
$data = 'abc' . PHP_EOL . 'def' . PHP_EOL;
file_put_contents(__DIR__ . '/file3.txt', $data, FILE_APPEND);