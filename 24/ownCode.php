<?php
/*
 * Выводим свой собственный код
 */
$fileName = __FILE__;                                       // имя самого скрипта
$file = fopen($fileName, 'r');                              // дескриптор файла
// Чтение кода из файла и вывод в браузер
//echo htmlspecialchars(file_get_contents($fileName));
while (!feof($file)) {
    echo htmlspecialchars(fgets($file));
    echo '<br>';
}
fclose($file);
echo '<br>';
echo '<a href="' . $_SERVER['HTTP_REFERER'] . '"><<< Назад</a>';
