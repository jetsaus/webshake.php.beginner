<?php
/*
 * Поиск и вывод всевозможных комбинации пар чисел
 */
require_once 'functions.php';
$strNum = '';                                                       // строка целых уникальных чисел
$arrStr = '';                                                       // массив строковых чисел
$arrNum = [];                                                       // массив целых уникальных чисел
// контроль ввода
if (!empty($_POST['strNum'])) {
    $strNum = $_POST['strNum'];
    echo 'Введена строка символов: "' . $strNum . '"<br><br>';
    $arrStr = explode(' ', trim($strNum));
    // Преобразование массива строковых данных в целочисленный с удалением недопустимых строковых и повторений
    $arrNum = array_map('intval', array_filter($arrStr, 'is_numeric'));
    $arrNum = array_unique($arrNum);                                                // только уникальные числа
    asort($arrNum);                                                                 // сортировка по возрастанию
    $arrNum = array_values($arrNum);                                                // переиндексация
    echo 'Числа для расчета: ';
    foreach ($arrNum as $value) {
        echo $value . ' ';
    }
    echo '<br><br>';
    echo 'Результат:' . '<br>';
    echo allCombinations($arrNum);
}else{
    echo 'Вы не ввели значения!';
    echo '<br><br>';
}

echo '<br><br>';
echo '<a href="' . $_SERVER['HTTP_REFERER'] . '"><<< Назад</a>';