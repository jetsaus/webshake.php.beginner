<?php
/*
 * Функция нахождения комбинаций чисел
 * @param   array   $arrNum массив          целых не повторяющихся символов
 * @return  string                          html-строка комбинаций пар чисел или сообщение об ошибке
 */
function allCombinations($arrNum = [])
{
    $strCombinations = '';
    if (count($arrNum) <= 1) {
        $strCombinations = 'единственный элемент не может образовать пару!';
        return $strCombinations;
    }
    for ($i=0;$i<count($arrNum);$i++) {
        $num = (string)$arrNum[$i];
        for ($j=0;$j<count($arrNum);$j++) {
            if ($i!=$j) {
                $strCombinations .= $num . ' ' . (string)$arrNum[$j] . '<br>';
            }
        }
    }
    return $strCombinations;
}
/*
 * Решение автора, более изящное!
 */
/*
$line = trim(fgets(STDIN));
$nums = explode(' ', $line);

foreach ($nums as $i => $num1) {
    foreach ($nums as $j => $num2) {
        if ($i === $j) {
            continue;
        }

        echo $num1 . ' ' . $num2 . PHP_EOL;
    }
}
*/