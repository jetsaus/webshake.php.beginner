<?php
/*
 * Обработчик калькулятора для домашнего задания
 */
if (empty($_GET)) {
    return 'Ничего не передано';
}
if (empty($_GET['operation'])) {
    return 'Операция не передана';
}

$x1 = $_GET['x1'] ?? null;
$x2 = $_GET['x2'] ?? null;
if (($x1 === null) && ($x2 === null)) {
    return 'Аргументы не переданы';
}
if ($x1 === null) {
    return 'Аргумент 1 не передан';
}
if ($x2 === null) {
    return 'Аргумент 2 не передан';
}

$operations = $_GET['operation'] ?? null;

$x1 = filter_input(INPUT_GET, 'x1', FILTER_VALIDATE_FLOAT);
$x2 = filter_input(INPUT_GET, 'x2', FILTER_VALIDATE_FLOAT);
if (($x1 === false) && ($x2 === false)) {
    return 'Аргументы не переданы или они "не-число"';
}
if ($x1 === false) {
    return 'Аргумент 1 не передан или он "не-число"';
}
if ($x2 === false) {
    return 'Аргумент 2 не передан или он "не-число"';
}

switch ($operations) {
    case '+':
        $result = $x1 + $x2;
        break;
    case  '-':
        $result = $x1 - $x2;
        break;
    case  '/':
        $result = $x2 !== 0.0 ? ($x1 / $x2) : 'На ноль делить нельзя';
        break;
    case  '*':
        $result = $x1 * $x2;
        break;
    default:
        return 'Операция не поддерживается или она не передана';
}
$expression = $x1 . ' ' . $operations . ' ' . $x2 . ' = ';
return $expression . $result;