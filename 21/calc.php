<?php
/*
 * Логика вычислений
 */

// Проверка корректности передачи аргументов
if (empty($_GET)) {
    return 'Ничего не передано!';
}
if (empty($_GET['operation'])) {
    return 'Не передана операция!';
}
/*if ((empty($_GET['x1'])&&($_GET['x1']!='0')) || (empty($_GET['x2'])&&($_GET['x2']!='0'))) {
    return 'Не передан один, либо все аргументы!';
}*/
// переписано с использованием null-coalasce оператора
$x1 = $_GET['x1'] ?? null;
$x2 = $_GET['x2'] ?? null;
if ($x1 == null || $x2 == null) {
    return 'Не передан один, либо все аргументы!';
}
//----------------------------------------------------
$x1 = (float)$x1;                   //  после использования null-collapse оператора
$x2 = (float)$x2;                   //  по логике не в том месте стоят
if ($_GET['x2'] === '0' && $_GET['operation'] === '/') {
    return 'На ноль делить нельзя!';
}
if (!is_numeric($_GET['x1']) || !is_numeric($_GET['x2'])) {
    return 'Переданные значения должны быть числом!';
}
$operation = $_GET['operation'];

$expression = $x1 . ' ' . $operation . ' ' . $x2 . ' = ';       // ыормирование отображения операции

switch ($operation) {                                           // собственно сами вычисления
    case '+':
        $result = $x1 + $x2;
        break;
    case '-':
        $result = $x1 - $x2;
        break;
    case '*':
        $result = $x1 * $x2;
        break;
    case '/':
        $result = $x1 / $x2;
        break;
    default:
        return 'Операция не поддерживается';
}

return $expression . $result;
