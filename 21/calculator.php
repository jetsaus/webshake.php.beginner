<!--Форма для ввода данных-->
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Калькулятор</title>
</head>
<body>
<form action="result.php" method="get">
    <input name="x1" type="number" step="any">      <!-- ввод только чисел -->
    <select name="operation">
        <option value="+">+</option>
        <option value="-">-</option>
        <option value="*">*</option>
        <option value="/">/</option>
    </select>
    <input name="x2" type="number" step="any">      <!-- ввод только чисел -->
    <input type="submit" value="Посчитать">
</form>
</body>
</html>