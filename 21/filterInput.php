<!--Форма для ввода данных с вызовом обработчика операций handler.php-->
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Калькулятор</title>
</head>
<body>
<form action="result.php" method="get">
    <input name="x1">
    <select name="operation">
        <option value="+">+</option>
        <option value="-">-</option>
        <option value="/">/</option>
        <option value="*">*</option>
    </select>
    <input name="x2">
    <input type="submit" value="Посчитать">
</form>
</body>
</html>