<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Замена произведением</title>
</head>
<body>
<h4>Задача:</h4>
<p>На вход подается строка из чисел, разделенных пробелами. Замените каждый элемент строки произведением всех других элементов.</p>
<p>Рассмотрим на примере строки "1 2 3". Новое значение вместо "1" это "2 * 3 = 6". Для "2" это "1 * 3 = 3". Для "3" это "1 * 2 = 2"<br>
Проверки на 0 не делается, т.к. этого нет в условии, но присутствие 0 в строке обнулит часть или весь результат.</p>
<h4>Решение:</h4>
<?php
require_once 'functions.php';
$strNumber = ' 5 3 7 2 1 ';                         // строка чисел
$arrNum = [];                                       // массив чисел
$arrBuf = [];                                       // промежуточный массив для расчета
echo '<p>Строка чисел: ' . $strNumber . '</p>';
$arrNum = explode(' ', trim($strNumber));
$arrBuf = $arrNum;
// расчет произведений
foreach ($arrNum as $keyOut=>$itemOut) {
    $mulNum = 1;
    foreach ($arrBuf as $keyIn=>$itemIn) {
        if ($keyOut != $keyIn) {
            $mulNum *= (float)$itemIn;
        }
    }
    $arrNum[$keyOut] = $mulNum;
}
$strNumber = implode(' ', $arrNum);
echo '<p>Произведение других элементов: ' . $strNumber . '</p>';
?>
</body>
</html>