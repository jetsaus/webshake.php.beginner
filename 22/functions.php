<?php
// Была необходима для отладки и тестирования
/** Функция перемножения всех элементов массива
 * @param   array   $array      массив для перемножения элементов
 * @return  float               произведение всех элементов массива
 */
function arrMul($array = []) : float
{
    $multiplication = 1;        // произведение всех элементов массива
    foreach ($array as $item) {
        $multiplication *= (int)$item;
    }
    return $multiplication;
}
