<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Максимальная разница</title>
</head>
<body>
<h4>Задача:</h4>
<p>На вход подается строка целых чисел, разделенных пробелами. Найдите максимальную разницу между двумя элементами строки при условии, что меньшее число должно находиться справа от большего.</p>
<p>Например, для строки "1 6 4 3" правильный ответ: "3" (6-3)</p>
<h4>Решение:</h4>
<?php
//require_once 'functions.php';
$maxItem = 0;                                       // максимальный элемент массива
$minItem = 0;                                       // минимальный элемент массива, находящийся справа от максимального
$keyMax = 0;                                        // индекс максимального элнмента
//$strNumber = ' 5 3 7 2 1 ';                         // строка чисел
//$strNumber = ' 1 6 4 3 ';                           // строка чисел
$strNumber = ' 777 83 10000 -10000 100000 ';                           // строка чисел
$arrNum = [];                                       // массив чисел как integer
$strArrNum = explode(' ', trim($strNumber));        // массив чисел как string
// преобразование из массива строк в массив целых
foreach ($strArrNum as $item) {
    $arrNum[] = (integer)$item;
}
$keyMax = 0;
$maxItem = min($arrNum);                              // максимальный элемент
foreach ($arrNum as $key=>$item) {
    if (($item>$maxItem)&&($item!=end($arrNum))) {
        $maxItem = $item;
        $keyMax = $key;
    }
}
$minItem = max($arrNum);                              // мминимальный элемент
foreach ($arrNum as $key=>$item) {
    if (($item<$minItem)&&($key>$keyMax)) {
        $minItem = $item;
    }
}
echo '<p>Строка чисел: ' . trim($strNumber) . '</p>';
echo '<p>Максимальная разница: ' . (string)($maxItem-$minItem) . '</p>';
?>
</body>
</html>