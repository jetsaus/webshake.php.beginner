<?php
/** Функция возвращает минимальное из трех чисел, полученных в виде аргументов
 * @param float $x          первое число
 * @param float $y          второе число
 * @param float $z          третье число
 * @return float            минимальное число из трех
 */
function minFloat(
    float $x,
    float $y,
    float $z
): float
{
    return min($x, $y, $z);
}
/** Функция принимает два аргумента по ссылкам и умножает их на 2
 * @param float $x      первое число
 * @param float $y      второе число
 */
function mulTwo(
    float &$x,
    float &$y
)
{
    $x = $x * 2;
    $y = $y * 2;
}
/** Функция вычисляет факториал числа с помощью рекурсии
 * @param   int $x      число
 * @return  int         факториал этого числа
 */
function factorial(
    int $x
): int
{
    if ($x === 0) return 1;
    return ($x * factorial($x -1 ));
}
/** Функция суммирует числа от 0 до заданного числа
 * @param   int $x      заданное число
 */
function outNumber(
    int $x
)
{
    if ($x === 0) {
        echo $x;
        return;
    }
    outNumber($x - 1);
    echo ', ' . $x;
}