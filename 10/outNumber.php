<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Вывод чисел</title>
</head>
<body>
    <h3>Вывод положительных чисел от 0 до переданного значения</h3>
    <?php
        include_once('functions.php');
        $x = 12;
        echo 'Дано число ';
        echo $x;
        echo '<br><br>';
        if (is_int($x) && $x>=0) {
            echo 'Целые числа от 0 до ' . $x . ' - ';
            outNumber($x);
        }else{
            echo 'Исходное число должно быть целым и положительным!';
        }
    ?>
</body>
</html>