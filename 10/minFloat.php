<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Минимум из трех float</title>
</head>
<body>
    <h3>Минимум из трех чисел типа float</h3>
    <span>Даны три числа:</span>
    <?php
        include_once('functions.php');
        $x = 3.14;
        $y = 2.48;
        $z = - 3.12;
        echo '$x = ' . $x . ', ';
        echo '$y = ' . $y . ', ';
        echo '$z = ' . $z;
    ?>
    <br><br>
    <span>Минимальное их этих чисел: </span>
    <?php
        echo minFloat($x, $y, $z);
    ?>
</body>
</html>