<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Умножение аргументов</title>
</head>
<body>
    <h3>Умножение двух аргументов на 2, переданных в функцию по ссылке</h3>
    <span>Даны два числа:</span>
    <?php
        include_once('functions.php');
        $x = 3.14;
        $y = 2.48;
        echo '$x = ' . $x . ', ';
        echo '$y = ' . $y;
    ?>
    <br><br>
    <span>Умножили каждое из чисел на 2:</span>
    <?php
        mulTwo($x, $y);
        echo '$x = ' . $x . ', ';
        echo '$y = ' . $y;

    ?>
</body>
</html>