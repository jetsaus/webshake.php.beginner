<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Факториал числа</title>
</head>
<body>
    <h3>Вычисление факториала числа</h3>
    <?php
        include_once('functions.php');
        $x = 5;
        echo 'Дано число ';
        echo $x;
        echo '<br><br>';
        if (is_int($x) && $x>=0) {
            echo $x . '! = ' . factorial($x);
        }else{
            echo 'Исходное число должно быть целым и положительным!';
        }
    ?>
</body>
</html>