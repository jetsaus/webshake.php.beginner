<?php
$pattern = '/^[0-9- ]+$/i';                                                 // шаблон чисел и пробелов
$arrStrNumber = [];                                                         // массив строковых чисел
$arrNumber = [];                                                            // массив целых чисел
$maxMult = 0;                                                               // максимальное произведение
$strResult = '';                                                            // результат

$strNumber = trim(!empty($_POST['strNumber']) ? $_POST['strNumber'] : '');  // получение данных
if (preg_match($pattern, $strNumber) && !empty($strNumber)) {               // допустимо ли введенное значение?
    // удаление лишних пробелов и преобразование в массив целых чисел $arrNumber
    $arrStrNumber = explode(' ', preg_replace("/\s{2,}/"," ",$strNumber));
    foreach ($arrStrNumber as $item) {
        $arrNumber[] = (int)($item);
    }
    // прогоняем массив с самим собой
    foreach ($arrNumber as $keyFirst => $mulFirst) {
        foreach ($arrNumber as $keySecond => $mulSecond) {
            // сравниваем произведение элементов с максимумом,
            // исключая произведение элемента с самим собой
            if ((($mulFirst * $mulSecond) > $maxMult) && ($keyFirst != $keySecond)) {
                $maxMult = $mulFirst * $mulSecond;
            }
        }
    }
    $strResult = (string)$maxMult;                                          // результат -> в строку
} else {
    $strResult = 'Введенная строка содержит недопустимые значения';
}
?>

<html lang="ru">
<head>
    <title>Максимальное произведение</title>
</head>
<body>
    <h4>Исходная строка:</h4>
        <?php echo $strNumber; ?>
    <h4>Максимальное произведение:</h4>
        <?php echo $strResult; ?>
</body>
</html>