<?php
$pattern = '/^[0-9- ]+$/i';                                                 // шаблон чисел и пробелов
$arrStrNumber = [];                                                         // массив строковых чисел
$arrNumber = [];                                                            // массив целых чисел
$arrCountNumber = [];                                                       // количество элементов массива
$strResult = '';                                                            // строка результата

$strNumber = trim(!empty($_POST['strNumber']) ? $_POST['strNumber'] : '');  // получение данных
if (preg_match($pattern, $strNumber) && !empty($strNumber)) {               // допустимо ли введенное значение?
    // удаление лишних пробелов и преобразование в массив целых чисел $arrNumber
    $arrStrNumber = explode(' ', preg_replace("/\s{2,}/"," ",$strNumber));
    foreach ($arrStrNumber as $item) {
        $arrNumber[] = (int)($item);
    }
    sort($arrNumber, SORT_NUMERIC);                                         // сортировка массива по возрастанию
    $arrCountNumber = array_count_values($arrNumber);                       // определение количества каждого элемента
    // вывод повторяющихся элементов
    foreach ($arrCountNumber as $itemNumber => $itemCount) {
        if ($itemCount > 1) {
            $strResult .= $itemNumber . ' ';
        }
    }
} else {
    $strResult = 'Введенная строка содержит недопустимые значения';
}
?>

<html>
<head>
    <title>Повторяющиеся значения</title>
</head>
<body>
    <h4>Исходная строка:</h4>
        <?php echo $strNumber; ?>
    <h4>Повторяющиеся значения:</h4>
        <?php echo $strResult; ?>
</body>
</html>