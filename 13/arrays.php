<?php
// Массив
$fruits = ['apple', 'orange', 'grape'];
$fruits[] = 'mango';
unset($fruits[2]);
$fruits[20] = 'grape';
$fruits[0] = 'banana';
echo '<pre>';
    var_dump($fruits);
echo '</pre>';

// Ассоциативный массив
$article = ['title' => 'Название статьи', 'text' => 'Текст статьи'];
$article['author'] = 'Имя автора';
echo '<pre>';
var_dump($article);
echo '</pre>';
?>
<!-- вывод элементов массива в браузер -->
<html>
<head>
    <title><?php echo $article['title']; ?></title>
</head>
<body>
    <h1><?php echo $article['title']; ?></h1>
    <p><?php echo $article['author']; ?></p>
    <p><?php echo $article['text']; ?></p>
</body>
</html>