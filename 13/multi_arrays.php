<?php
// Многомерный массив
$article = [
        'title' => 'Название статьи',
        'text' => 'Текст статьи',
        'author' => [
                'first_name' => 'Иван',
                'last_name' => 'Иванов',
        ]
];
echo '<pre>';
var_dump($article);
echo '</pre>';
?>
<!-- вывод элементов массива в браузер -->
<html>
<head>
    <title><?php echo $article['title']; ?></title>
</head>
<body>
    <h1><?php echo $article['title']; ?></h1>
    <p><?php echo $article['author']['first_name'] .' ' . $article['author']['last_name']; ?></p>
    <p><?php echo $article['text']; ?></p>
</body>
</html>