<?php
// Контент сайта
$content = '<h3>Заголовок статьи</h3><p>Текст статьи</p>';

// Заголовок сайта
$head_1 = '<h2>';
$head_2 = 'Это заголовок моего сайта';
$head_3 = '</h2>';
$currentDate = 'Сегодня ' . date('d.m.Y');

// Меню сайта
$menu_1 = '<a href="#">Меню 1</a><br>';
$menu_2 = '<a href="#">Меню 2</a><br>';
$menu_3 = '<a href="#">Меню 3</a><br>';
$menu_4 = '<a href="#">Меню 4</a><br>';

// Подвал сайта
$footer_1 = 'Я в сети:';
$footer_2 = '<a href="#">OK</a>';
$footer_3 = '<a href="#">VK</a>';
$footer_4 = '<a href="#">FB</a>';

require __DIR__ . '/header.php';
require __DIR__ . '/sidebar.php';
require __DIR__ . '/content.php';
require __DIR__ . '/footer.php';
