<?php
/** Функция возвращает истину/ложь, если число четное
 * @param int $x    число, проверяемое на четность
 * @return boolean
 */
function isEven(int $x) : bool
{
    return (($x % 2) === 0) ? true : false;
}
