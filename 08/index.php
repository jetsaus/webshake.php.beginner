<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Операторы в PHP</title>
</head>
<body>
<h3>Задачи:</h3>
<p>
   1.Самостоятельно изучите официальную документацию на тему "приоритетов операторов"
    <br>
   2.С помощью функции var_dump() получите результаты выражений
</p>
<h3>Решение:</h3>
<p>
    1.Изучен раздел документации <a href="http://php.net/manual/ru/language.operators.precedence.php" target="_blank">приоритеты операторов</a>
    <br>
    2.Результаты выражений: <br><br>
        var_dump(!1) = <?php echo var_dump(!1) . '<br>'; ?>
        var_dump(!0) = <?php echo var_dump(!0) . '<br>'; ?>
        var_dump(!true) = <?php echo var_dump(!true) . '<br>'; ?>
        var_dump(2 && 3) = <?php echo var_dump(2 && 3) . '<br>'; ?>
        var_dump(5 && 0) = <?php echo var_dump(5 && 0) . '<br>'; ?>
        var_dump(3 || 0) = <?php echo var_dump(3 || 0) . '<br>'; ?>
        var_dump(5 / 1) = <?php echo var_dump(5 / 1) . '<br>'; ?>
        var_dump(1 / 5) = <?php echo var_dump(1 / 5) . '<br>'; ?>
        var_dump(5 + '5string') = <?php echo var_dump(5 + '5string') . '<br>'; ?>
        var_dump('5' == 5) = <?php echo var_dump('5' == 5) . '<br>'; ?>
        var_dump('05' == 5) = <?php echo var_dump('05' == 5) . '<br>'; ?>
        var_dump('05' == '5') = <?php echo var_dump('05' == '5') . '<br>'; ?>
</p>
</body>
</html>