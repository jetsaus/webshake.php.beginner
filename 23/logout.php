<?php
/*
 * Выход из системы
 */
// Если куки аутентификации установлены, удаляем их и переходим на главную страницу
if (isset($_COOKIE['login']) && isset($_COOKIE['password'])) {
    setcookie('login', '', time()-3600, '/');
    setcookie('password', '', time()-3600, '/');
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}