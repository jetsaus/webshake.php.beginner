<?php
require_once __DIR__ . '/functions.php';
$login = getUserLogin();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Главная страница</title>
</head>
<body>
<?php if ($login === null): ?>
    <a href="login.php">Авторизуйтесь</a>
<?php else: ?>
     Добро пожаловать, <?php echo $login ?>
    <br>
    <a href="logout.php" onclick="return  confirm('Выйти из системы?')">Выйти</a>
<?php endif; ?>
</body>
</html>
