<?php
/*
 * Модуль аутентификации
 */
/** Функция проверки аутентификации пользователя
 * @param   string  $login      логин пользователя
 * @param   string  $password   пароль пользователя
 * @return  bool                логическое значение прошел/не прошел аутентификацию
 */
function checkAuth(string $login, string $password): bool
{
    $users = require __DIR__ . '/usersDB.php';
    foreach ($users as $user) {
        if ($user['login'] === $login && $user['password'] === $password) {
            return true;
        }
    }
    return false;
}
/** Функция получения логина/пароля из базы пользователей
 * @return  string      значение логина или null, если такого пользователя не существует
 */
function getUserLogin(): ?string
{
    $loginFromCookie = $_COOKIE['login'] ?? '';
    $passwordFromCookie = $_COOKIE['password'] ?? '';
    if (checkAuth($loginFromCookie, $passwordFromCookie)) {
        return $loginFromCookie;
    }
    return null;
}
/** Функция расчета максимальной суммы подряд идущих чисел
 * @param   array   $arr    логин пользователя
 * @return  integer         максимальная сумма подряд идущих чисел
 */
function maxSecSum($arr = [])
{
    $maxSoFar = $maxEndIngHere = $arr[0];
    for ($i=1; $i<count($arr); $i++) {
        $maxEndIngHere += $arr[$i];
        if ($maxEndIngHere < $arr[$i]) {
            $maxEndIngHere = $arr[$i];
        }
        $maxSoFar = max($maxSoFar, $maxEndIngHere);
    }
    return $maxSoFar;
}
