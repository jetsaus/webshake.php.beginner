<?php
require_once 'functions.php';
$strNum = (!empty($_POST['strNum']) || ($_POST['strNum'])==='0') ? $_POST['strNum'] : '';
// Контроль введения данных
if (($strNum !== '')) {
    $arrStr = explode(' ', trim($strNum));
    // Преобразование массива строковых данных в целочисленный с удалением недопустимых строковых и переиндексацией
    $arrNum = array_map('intval', array_filter($arrStr, 'is_numeric'));
    $arrNum = array_values($arrNum);
    if (!empty($arrNum)) {
        echo 'Массив целых чисел: ';
        foreach ($arrNum as $value) {
            echo $value . ' ';
        }
        echo '<br><br>';
        echo 'Максимальная сумма подряд идущих чисел: ' . maxSecSum($arrNum);
    }else{
        echo 'Возможно вы не ввели числа!';
    }
}else{
    echo 'Вы ничего не ввели!';
}
