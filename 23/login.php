<?php
/*
 * Страница входа в систему
 */
if (!empty($_POST)) {
    require __DIR__ . '/functions.php';
    $login = $_POST['login'] ?? '';
    $password = $_POST['password'] ?? '';

    if (checkAuth($login, $password)) {
        setcookie('login', $login, 0, '/');
        setcookie('password', $password, 0, '/');
        header('Location: index.php');
    } else {
        $error = 'Ошибка авторизации';
    }
}
?>
<!DOCTYPE html>
<!-- Форма автризации -->
<html lang="ru">
<head>
    <title>Форма авторизации</title>
</head>
<body>
<?php if (isset($error)): ?>
    <span style="color: red;">
    <?php echo $error ?>
</span>
<?php endif; ?>
<?php
    // Проверка авторизирован ли пользователь ранее под своими аутентификационными данными
    require_once __DIR__ . '/functions.php';
    if (isset($_COOKIE['login']) && isset($_COOKIE['password']) && checkAuth($_COOKIE['login'], $_COOKIE['password'])) {
        header('Location: index.php');
    }
?>
<form action="login.php" method="post">
    <label for="login">Имя пользователя: </label><input type="text" name="login" id="login">
    <br>
    <label for="password">Пароль: </label><input type="password" name="password" id="password">
    <br>
    <input type="submit" value="Войти">
</form>
</body>
</html>
