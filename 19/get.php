<?php
/* Параметры GET-запроса из адресной строки браузера */
var_dump($_GET);

$login = !empty($_GET['login']) ? $_GET['login'] : 'логин не передан!';
$password = !empty($_GET['password']) ? $_GET['password'] : 'пароль не передан!';
?>
<html>
    <head>
        <title>Знакомство с GET-запросами</title>
    </head>
<body>
    <p>
        Переданный логин: <?php echo $login ?>
        <br>
        Переданный пароль: <?php echo $password ?>
    </p>
</body>
</html>