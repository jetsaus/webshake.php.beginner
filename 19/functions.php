<?php
/** Функция определения, является ли пара слов анаграммой
 * @param   string    word_1        первое слово пары
 * @param   string    word_2        второе слово пары
 * @return  boolean                 анаграмма или нет
 */
function anagram($word_1 = '', $word_2 = '') : bool
{
    $isAnagram = true;                                      // это анаграмма?
    if (mb_strlen($word_1) != mb_strlen($word_2)) {         // строки не равны по количеству символов
        $isAnagram = false;
        return $isAnagram;
    }
    // все символы строки в верхний регистр и в массивы символов
    $arrStrOne = preg_split('//u', mb_strtoupper($word_1));
    $arrStrTwo = preg_split('//u', mb_strtoupper($word_2));
    // сортируем по алфавиту оба массива
    sort($arrStrOne);
    sort($arrStrTwo);
    // сравниваем символы слов
    $i = 0;
    foreach ($arrStrOne as $item) {
        // символы не совпадают, уже не анаграмма
        if ($item !== $arrStrTwo[$i]) {
            $isAnagram = false;
        }
        $i++;
    }
    return $isAnagram;
}
