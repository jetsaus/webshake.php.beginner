<?php
if (!empty($_FILES)) {
    $file = $_FILES['attachment'];
    // собираем путь до нового файла - папка uploads в текущей директории
    // в качестве имени оставляем исходное имя файла во время загрузки в браузере
    $srcFileName = $file['name'];                                           // имя загружаемого файла
    $newFilePath = __DIR__ . '/uploads/' . $srcFileName;                    // руть загрузки в файловой системе
    $allowedExtensions = ['jpg', 'png', 'gif'];                             // разрешенные типы файлов
    $extension = pathinfo($srcFileName, PATHINFO_EXTENSION);                // тип файла
    $notError = true;                                                       // флаг ошибки

    // Проверка на ошибки и запреты
    if (!in_array($extension, $allowedExtensions)) {                        // разрешенные к загрузке типы файлов
        $notError = false;
        $error = 'Загрузка файлов с таким расширением запрещена!';
    } elseif ((getimagesize($file['tmp_name'])[0] > 1280) ||                // ограничение размера картинки
        (getimagesize($file['tmp_name'])[1] > 720)) {
        $notError = false;
        $error = 'Отказано в загрузке, ограничения на размер картинки: ширина <= 1280px, высота <= 720px' . '<br>';
        $error .= 'Ваш файл имеет размер ' . getimagesize($file['tmp_name'])[0] . 'px на ';
        $error .= getimagesize($file['tmp_name'])[1] . 'px';
    } elseif (file_exists($newFilePath)) {
        $notError = false;
        $error = 'Файл с таким именем уже существует!';
     } elseif ($file['error'] !== UPLOAD_ERR_OK) {                          // неустановленная ошибка при загрузке файла
        $notError = false;
        $error = 'Ошибка при загрузке файла!';
    } elseif (!move_uploaded_file($file['tmp_name'], $newFilePath)) {       // загрузка файла
        $notError = false;
        $error = 'Ошибка при загрузке файла!';
    }
    if ($notError) {                                                        // загрузка без ошибок
        echo $result = 'Загружен http://wsh/php.beginner/25/uploads/' . $srcFileName;
    } else {
        echo $result = $error;
    }
}
?>

<html lang="ru">
<head>
    <title>Загрузка файла</title>
</head>
<body>
<form action="<?php __DIR__ . '/upload.php'; ?>" method="post" enctype="multipart/form-data">
    <input type="file" name="attachment">
    <br>
    <input type="submit">
</form>
</body>
</html>