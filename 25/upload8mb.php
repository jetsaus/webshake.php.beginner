<?php
if (!empty($_FILES)) {
    $file = $_FILES['attachment'];
    // собираем путь до нового файла - папка uploads в текущей директории
    // в качестве имени оставляем исходное имя файла во время загрузки в браузере
    $srcFileName = $file['name'];                                   // имя загружаемого файла
    $newFilePath = __DIR__ . '/uploads/' . $srcFileName;            // руть загрузки в файловой системе
    $allowedExtensions = ['jpg', 'png', 'gif', 'pdf'];              // разрешенные типы файлов
    $extension = pathinfo($srcFileName, PATHINFO_EXTENSION);        // тип загружаемого файла
    $notError = true;                                               // флаг ошибки
    // Проверка на ошибки и запреты
    if (!in_array($extension, $allowedExtensions)) {
        $notError = false;
        $error = 'Загрузка файлов с таким расширением запрещена!';
    } elseif ($_FILES['attachment']['size'] >= 8000000) {           // файл 8Mb или свыше
        $notError = false;
        $error = 'Запрещено загружать файлы размером свыше 8Mb';
    } elseif (file_exists($newFilePath)) {
        $notError = false;
        $error = 'Файл с таким именем уже существует!';
     } elseif ($file['error'] !== UPLOAD_ERR_OK) {                   // неустановленная ошибка при загрузке файла
        $notError = false;
        $error = 'Ошибка при загрузке файла!';
    } elseif (!move_uploaded_file($file['tmp_name'], $newFilePath)) {   // загрузка файла
        $notError = false;
        $error = 'Ошибка при загрузке файла!';
    }
    if ($notError) {                                                // загрузка без ошибок
        echo $result = 'Загружен http://wsh/php.beginner/25/uploads/' . $srcFileName;
    } else {
        echo $result = $error;
    }
}
?>

<html lang="ru">
<head>
    <title>Загрузка файла</title>
</head>
<body>
<form action="<?php __DIR__ . '/upload.php'; ?>" method="post" enctype="multipart/form-data">
    <input type="file" name="attachment">
    <br>
    <input type="submit">
</form>
</body>
</html>