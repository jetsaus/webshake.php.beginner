
<?php
// Контроль ввода данных в форму и вывод результата
/*
 * Описание используемых переменных
    $strInt;                                                            // строка целых уникальных чисел
    $arrStr;                                                            // массив строковых чисел
    $arrInt;                                                            // массив целых чисел
    $k;                                                                 // количество комбинаций
*/
require_once 'engine/functions.php';
if (!empty($_POST['strInt']) && !empty($_POST['countCombine'])) {   // контроль ввода не пустых данных
    $k = (integer)$_POST['countCombine'];
    $strInt = $_POST['strInt'];
    $arrInt = transformStrToInt($strInt);                           // трансформация строки в массив integer
    $arrInt = array_unique($arrInt);                                // только уникальные числа
    if (!empty($arrInt) && $k>=1 && $k<=count($arrInt)) {           // контроль корректности ввода
        asort($arrInt);                                             // сортировка по возрастанию
        $arrInt = array_values($arrInt);                            // переиндексация
        // Здесь вызов функции обработки, которая возвращает многомерный массив результата
        comGivLen($arrInt, $k);
        var_dump($arrInt);
        var_dump($k);
    }else{
        echo '<br>Введите корректные числовые данные!';
    }
}else{
    echo '<br>Введите необходимые числовые данные!';
}
?>