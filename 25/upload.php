<?php
if (!empty($_FILES)) {
    var_dump($_FILES);
    $file = $_FILES['attachment'];
    // собираем путь до нового файла - папка uploads в текущей директории
    // в качестве имени оставляем исходное имя файла во время загрузки в браузере
    $srcFileName = $file['name'];                                   // имя загружаемого файла
    $newFilePath = __DIR__ . '/uploads/' . $srcFileName;            // руть загрузки в файловой системе
    $allowedExtensions = ['jpg', 'png', 'gif'];                     // разрешенные типы файлов
    $extension = pathinfo($srcFileName, PATHINFO_EXTENSION);        //
    $notError = true;                                               // флаг ошибки
    // Проверка на ошибки и запреты
    if (!in_array($extension, $allowedExtensions)) {
        $fError = false;
        echo $error = 'Загрузка файлов с таким расширением запрещена!';
    } elseif ($file['error'] !== UPLOAD_ERR_OK) {
        $fError = false;
        echo $error = 'Ошибка при загрузке файла!';
    } elseif (file_exists($newFilePath)) {
        $fError = false;
        echo $error = 'Файл с таким именем уже существует!';
    } elseif (!move_uploaded_file($file['tmp_name'], $newFilePath)) {
        $fError = false;
        echo $error = 'Ошибка при загрузке файла!';
    }
    if ($notError) {                                                // загрузка без ошибок
        echo $result = 'Загружен http://wsh/php.beginner/25/uploads/' . $srcFileName;
    }
}
?>

<html>
<head>
    <title>Загрузка файла</title>
</head>
<body>
<form action="<?php __DIR__ . '/upload.php'; ?>" method="post" enctype="multipart/form-data">
    <input type="file" name="attachment">
    <input type="submit">
</form>
</body>
</html>