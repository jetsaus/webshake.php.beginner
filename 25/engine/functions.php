<?php
/*
 * Функция преобразования строки целых чисел, разделенных пробелами в массив целых чисел integer
 * Если чисел не введено, возвращает пустой массив
 * Символы и цифры с символами считаются символами и отбрасываюся, преобразованию не подлежат, например 'd', '5a', 'f7'
 * @param   string  $strInt                 строка целых чисел, разделенных пробелами
 * @return  array   $arrInt                 массив целых чисел типа integer
 */
function transformStrToInt(
    $strInt = ''                                                            //строка целых чисел, разделенных пробелами
):array                                                                     //массив целых чисел типа integer
{
    $arrInt = [];                                                           //массив целых чисел типа integer
    $arrStr = explode(' ', trim($strInt));
    $arrInt = array_map('intval', array_filter($arrStr, 'is_numeric'));     //преобразование в массив integer
    return $arrInt;
}
/*
 * Функция поиска всех возможных комбинаций заданной длины
 * param    array   $arrInt                 массив целых уникальных чисел
 * param    integer $k                      количество комбинаций
 * @return  string  $htmlCombination        html-строка комбинаций чисел или сообщение об ошибке
 */
function comGivLen(
    $arrInt = [],                           // массив целых уникальных чисел
    $k = 0                                  // количество комбинаций
):string
{
    $htmlCombination = '';                                                      // html-код решения
    if (empty($arrInt) || $k<1 || $k>count($arrInt)) {                          // контроль корректности данных
        return $htmlCombination = '<br>Введите корректные данные';
    }
    $countRow = pow(count($arrInt), $k);                                        // количество строк в $arrSolution
    $countColumn = $k;                                                          // количество колонок в $arrSolution
    $arrSolution = array_fill(0, $countRow, array_fill(0, $countColumn, null)); // пустой массив решения

    echo '!';
/*
  foreach ($nums as $i => $num1) {
    foreach ($nums as $j => $num2) {
        if ($i === $j) {
            continue;
        }
        echo $num1 . ' ' . $num2 . PHP_EOL;
    }
  }
*/


    return $htmlCombination;
}