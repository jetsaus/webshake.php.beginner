<?php
// Примеры цикла for

echo 'Обычный for:' . '<br>';
for ($i = 0; $i < 100; $i++) {
    echo $i . ' ';
}
echo '<br><br>';

echo 'Инициализация итератора вне тела цикла:' . '<br>';
$i = 0;
for (; $i < 100; $i++) {
    echo $i . ' ';
}
echo '<br><br>';

echo 'Инициализация инкремента итератора вне тела цикла:' . '<br>';
$i = 0;
for (; $i < 100;) {
    echo $i . ' ';
    $i++;
}
echo '<br><br>';

// Бесконечный цикл
/*
for (;;) {
    //этот код будет выполняться бесконечно
}
*/

echo 'Вывод всех четных чисел от 0 до 50: <br>';
for ($i=0; $i<=50; $i++) {
    if ($i % 2 == 0) {
        echo $i . ' ';
    }
}
echo '<br><br>';

echo 'Создать массив из 50 случайных значений: <br>';
for ($i=0; $i<=50; $i++) {
    $arrayRand[] = mt_rand(0, 100);
}
foreach ($arrayRand as $randNum) {
    echo $randNum . ' ';
}
echo '<br><br>';
echo 'Посчитать сумму чисел от 1 до 1000: ';
$sumNum = 0;
for ($i=0; $i<=100; $i++) {
    $sumNum += $i;
}
echo $sumNum;

