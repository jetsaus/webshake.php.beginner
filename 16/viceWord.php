<?php
/* Изменение порядка слов на обратный */

echo '<title>Изменение порядка</title>';
echo '<p>
Дана входная строка со словами. Измените порядок слов в обратном порядке. Словом считается любая последовательность НЕпробельных символов. Входная строка не содержит пробелов в начале и конце. Каждое слово отделено от другого одним пробелом.
</p>';

$string = 'Новый год - всемирный праздник !';   // исходная строка
$arrStr[] = explode(" ", $string);              // массив слов строки

echo '<p>Исходная строка:</p>';
echo $string . '<br><br>' ;

echo 'Результат преобразования:';
echo '<br><br>';
$string = '';
for ($i=count($arrStr[0]); $i>0; $i--) {        // перестановка слов
    $string .= $arrStr[0][$i-1] . ' ';
}
echo $string;