<?php
/* Перевод символов строки в верхний регистр */

echo '<title>Upper case</title>';
$string = 'Christmas is an international holiday!'; // исходная строка
$arrStr=[];                                         // массив для преобразований
echo '<p>Исходная строка:</p>';
$arrStr = str_split($string);                       // преобразование строки в массив
for($i=0; $i<strlen($string); $i++) {               // вывод исходной строки массивом
    echo $arrStr[$i];
}

$string = '';                                       // обнуление строки
echo '<p>Преобразованная строка:</p>';
for($i=0; $i<count($arrStr); $i++) {                // преобразование в uppercase
     $string .= mb_strtoupper($arrStr[$i], 'UTF-8');
}
echo $string;