<?php
// Цикл foreach в PHP
echo '<title>Цикл foreach в PHP</title>';
echo '<h3>Цикл foreach в PHP</h3>';
echo '<p>1.Придумайте способ обойтись без использования функции count.</p>';

$carsSpeeds = [
    95,
    140,
    78
];
echo '<pre>';
    var_dump($carsSpeeds);
echo '</pre>';
echo '<p>Скорости автомобилей:</p>';
foreach ($carsSpeeds as $speed) {
    echo $speed . ' км/ч' . '<br>';
}
echo '<br>';
$countOfCars = 0;                           // количество автомобилей
$sumOfSpeed = 0;
foreach ($carsSpeeds as $speed) {
    $sumOfSpeed += $speed;                  // суммируем скорости
    $countOfCars++;                         // инкрементируем количество автомобилей
}
echo 'Количество автомобилей - ' . $countOfCars . '<br>';
$averageSpeed = $sumOfSpeed / $countOfCars;
echo '<br>';
echo 'Средняя скорость автомобилей ' . $averageSpeed . ' км/час.';
echo '<br><br>';
echo 'Способ обойтись без count() - инкремент переменной в теле цикла.';
