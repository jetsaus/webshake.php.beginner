<?php
// Циклы
$carsSpeeds = [
    95,
    140,
    78
];
echo '<pre>';
    var_dump($carsSpeeds);
echo '</pre>';
foreach ($carsSpeeds as $speed) {
    echo $speed . '<br>';
}

$sumOfSpeed = 0;
foreach ($carsSpeeds as $speed) {
    $sumOfSpeed += $speed;
}
$countOfCars = count($carsSpeeds);
$averageSpeed = $sumOfSpeed / $countOfCars;
echo '<br>';
echo 'Средняя скорость автомобилей ' . $averageSpeed . ' км/час';
