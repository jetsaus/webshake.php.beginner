<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Условия в PHP</title>
</head>
<body>
<h3>Задачи:</h3>
<p>
    <span>1.Попробуйте следующие условия:</span>
    <ul>
        <li>if ('string') {echo 'Условие выполнилось';}</li>
            <?php
                if ('string') {echo 'Условие выполнилось';}
                var_dump('string');
            ?>
        <li>if (0) {echo 'Условие выполнилось';}</li>
            <?php
                if (0) {echo 'Условие выполнилось';}
                var_dump(0);
            ?>
        <li>if (null) {echo 'Условие выполнилось';}</li>
            <?php
                if (null) {echo 'Условие выполнилось';}
                var_dump(null);
            ?>
        <li>if (5) {echo 'Условие выполнилось';}</li>
            <?php
                if (5) {echo 'Условие выполнилось';}
                var_dump(5);
            ?>
    </ul>
<p>
</p>
    <span>2.С помощью тернарного оператора определите, является ли число чётным или нечётным и выведите результат.</span>
</p>

<h3>Решение:</h3>
<p>
    <span>1.Объяснение результата:</span>
    <br><br>
    <span>В условии нет операнда сравнения, поэтому все условия выполняются как истина.</span>
    <br><br>
    <span>2.Определение четности числа с помощью тернарного оператора:</span>
    <br><br>
    <?php
        $x = 3;
        echo '$x = ' . $x . '<br><br>';
        $x % 2 === 0  ? $remainder = 'четное' : $remainder = 'нечетное';
        echo $x . ' это ' . $remainder . ' число';
    ?>
</p>
</body>
</html>